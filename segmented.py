import pandas as pd
import numpy as np
from typing import List
from collections import namedtuple
import plotly.graph_objs as go

def plotNotebook(xdata, yinput, yfilter):
    input_data = go.Scatter(x=xdata[0], y=yinput[0], mode='lines', name=yinput[1], line_color='deepskyblue')
    filter_data = go.Scatter(x=xdata[0], y=yfilter[0], name=yfilter[1], line_color='orange')

    fig = go.Figure()
    fig.add_trace(input_data)
    fig.add_trace(filter_data)

    timeRange = [str(xdata[0][0]), str(xdata[0][len(xdata[0])-1])]
    fig.update_layout(title_text='Beam and Filter', height=600, width=1000,
                    xaxis_rangeslider_visible=True, 
                    xaxis=go.layout.XAxis(
                        autorange=True,
                        range=timeRange,
                        rangeslider=dict(
                            autorange=True,
                            range=timeRange),
                        type="date"),
                    yaxis=dict(title=yinput[1], color='deepskyblue'))
    fig.show()

SegmentedFilter = namedtuple('SegmentedFilter', ['start', 'end', 'a', 'b', 'c', 'd'])
inPeriod = lambda time, start, end: (time >= start) & (time < end)

def getIndexBetween(df: pd.DataFrame, columnName: str, start: str, end: str) -> List[int]:
    return df[inPeriod(df[columnName],  pd.Timestamp(start),  pd.Timestamp(end))].index

def filterSumFunction(data, filterData, i, a, b, c, d):
    result = 0.0
    for j in range(i-c+1, i+1):
        result += (a*data[j] + b + (1-d)*filterData[i-1])
    return result * (d/c)

def applyFilter(df: pd.DataFrame, timeColumnName: str, data: List[float], filterData: List[float], segfilter: SegmentedFilter) -> List[float]:
    indexRange = getIndexBetween(df, columnName=timeColumnName, start=segfilter.start, end=segfilter.end)
    for i in indexRange:
        filterData[i] = filterSumFunction(data, filterData, i, segfilter.a, segfilter.b, segfilter.c, segfilter.d)
    return filterData

def applyFilters(df: pd.DataFrame, timeColumnName: str, data: List[float], filterData: List[float], filters: List[SegmentedFilter]) -> List[float]:
    for segFilter in filters:
        filterData = applyFilter(df, timeColumnName, data, filterData, segFilter)
    return filterData

def getFiltersDf(timeColumnName: str, timestamps, measurementColumnName: str, measurement: List[float], filters: List[float]) -> pd.DataFrame:
    df = pd.DataFrame()
    df[timeColumnName] = timestamps
    df[measurementColumnName] = measurement
    df[measurementColumnName+'-segmented'] = filters
    return df
