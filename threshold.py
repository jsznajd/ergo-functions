import pandas as pd
import numpy as np
from typing import List, Dict
from collections import namedtuple
import plotly.graph_objs as go

def setRange(arr, val, start, end):
    end = min(len(arr) - 1, end)
    for i in range(start, end+1):
        arr[i] = val

def move(arr, thr, start, function):
    i = 0
    while (start + i) < len(arr) and function(arr[start + i], thr):
        i += 1
    return i-1

def thresholdFilter(data: List[float], threshold: float, lagBefore: int, lagAfter: int, timeOver: int, timeBelow: int) -> List[float]:
    result = np.zeros(len(data))
    
    i = 0
    while i < len(data):
        val = data[i]
        if val >= threshold:
            over = move(data, threshold, i, lambda value,thr: value >= thr)
            if over >= timeOver:
                start = max(i - lagBefore, 0)
                end = i + over + lagAfter
                setRange(result, 1, start, end)
            i += over
        else:
            below = move(data, threshold, i, lambda value,thr: value < thr)
            if below < timeBelow and result[max(i-1, 0)] == 1:
                start = i
                end = i + below
                setRange(result, 1, start, end)
            else:
                start = i + lagAfter if result[max(i-1, 0)] == 1 else i
                end = i + below
                setRange(result, 0, start, end)
            i += below
        i += 1
    return result

def plotNotebook(xdata, yinput, ythreshold, yfilter):
    input_data = go.Scatter(x=xdata[0], y=yinput[0], mode='lines', name=yinput[1], line_color='deepskyblue')
    filter_data = go.Scatter(x=xdata[0], y=yfilter[0], yaxis='y2', name=yfilter[1], line_color='orange')
    threshold_data = go.Scatter(x=xdata[0], y=ythreshold[0], name=ythreshold[1], opacity=0.7, line_color='red')

    fig = go.Figure()
    fig.add_trace(input_data)
    fig.add_trace(filter_data)
    fig.add_trace(threshold_data)

    timeRange = [str(xdata[0][0]), str(xdata[0][len(xdata[0])-1])]
    fig.update_layout(title_text='Beam and Filter', height=600, width=1000,
                    xaxis_rangeslider_visible=True, 
                    xaxis=go.layout.XAxis(
                        autorange=True,
                        range=timeRange,
                        rangeslider=dict(
                            autorange=True,
                            range=timeRange),
                        type="date"),
                    yaxis=dict(title=yinput[1], color='deepskyblue'),
                    yaxis2=dict(title=yfilter[1], color='orange', overlaying='y', side='right'))
    fig.show()

ThresholdFilter = namedtuple('ThresholdFilter', ['name', 'value', 'lagBefore', 'lagAfter', 'timeOver', 'timeBelow'])

def getFiltersForData(df: pd.DataFrame, dataProperties: List[ThresholdFilter]) -> pd.DataFrame:
    for thrProps in dataProperties:
        df[thrProps.name+'-threshold'] = thresholdFilter(df[thrProps.name], thrProps.value,
                                                        thrProps.lagBefore, thrProps.lagAfter,
                                                        thrProps.timeOver, thrProps.timeBelow)
    return df

def exportDf(df: pd.DataFrame, path: str) -> None:
    df.to_csv(path, index=False)
 