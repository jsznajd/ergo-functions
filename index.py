#%%[markdown]
## ERGO FUNCTIONS - prototype
#
# This document consists of *cells* which contain code - to run them simply press ```ctrl + enter``` on the active cell.<br />
# Results from cells are kept in the memory, so you don't have to re-run all of them from the begining.<br />
# You can use arrows to move between cells.
#
# ## Usage
# * **Right click and "Open Link in New Tab" on the *Jupyter* logo in the upper right corner.**
# * To input data, you must export it from excel, preferably only with columns : ```timestamp and measurements``` in CSV format.
# * Upload exported CSV file to the ***resources*** folder and set path to it in next steps. (see example file in the folder)
# * You can export filtered data to the CSV file, and use it in the excel.
# * Download exported results before closing web browser.
#
#%%[markdown]
# ## Setup 1.
# ### Run this cell first to setup the environment
#%%
import threshold as th
import segmented as sg
import pandas as pd
#%%[markdown]
# # Threshold filter
# ## Setup 
# ### Set parameters:
# * Path to the file with measurements
# * Name of the column containing timestamps
# * Replacement for missing measurements
#%%[markdown]
# ### Run cell
#%% Set and prepare values
dataPath = 'resources/example.csv'
timeColumnName = 'Timestamp'
emptyValueReplacement = 0.01

df = pd.read_csv(dataPath)
df[timeColumnName] = pd.to_datetime(df[timeColumnName], dayfirst=True)
df = df.fillna(emptyValueReplacement)
#%%[markdown]
# ### Set filter parameters.
#%% Set filter parameters
dataThresholds = [
    th.ThresholdFilter('PMIBISMV', value=0.09, lagBefore=5, lagAfter=4, timeOver=0, timeBelow=7),
    th.ThresholdFilter('PMIPS16', value=0.225, lagBefore=5, lagAfter=4, timeOver=0, timeBelow=7),
    th.ThresholdFilter('PMITT101', value=0.001, lagBefore=4, lagAfter=2, timeOver=0, timeBelow=3),
    th.ThresholdFilter('PMI201', value=0.00015, lagBefore=4, lagAfter=2, timeOver=0, timeBelow=3),
    th.ThresholdFilter('PMIL7412', value=0.000002, lagBefore=4, lagAfter=2, timeOver=0, timeBelow=3)
]
#%%[markdown]
# ### Run to calulate threshold filters.
#%% Get filter
df = th.getFiltersForData(df, dataThresholds)
#%%[markdown]
# ### Run to plot threshold filters.
#%% Plot filters
for thrProps in dataThresholds:
    th.plotNotebook(xdata=(df[timeColumnName], 'time'),
                    yinput=(df[thrProps.name], thrProps.name),
                    ythreshold=([thrProps.value for i in range(len(df))], 'threshold'),
                    yfilter=(df[thrProps.name+'-threshold'], thrProps.name+'-filter'))
#%%[markdown]
# ### Set path to export file and run cell.
#%% Export filters
th.exportDf(df, 'resources/threshold-result-example.csv')
#%%[markdown]
# # Segmented filter
# ## Setup 
# ### Set parameters:
# * Path to the file with measurements
# * Name of the column containing timestamps
# * Name of the column containing measurements (single channel)
# * Replacement for missing measurements
#%%[markdown]
# ### Run cell
#%% Set and prepare values
dataPath = 'resources/example.csv'
timeColumnName = 'Timestamp'
measurementColumnName = 'PMIPS16'
emptyValueReplacement = 0.005

df = pd.read_csv(dataPath)
df[timeColumnName] = pd.to_datetime(df[timeColumnName], dayfirst=True)
df = df.fillna(emptyValueReplacement)
#%%[markdown]
# ### Set filter parameters.
# * Set filter segments
# * Time format : ```yyyy-mm-dd hh:mm:ss```
# * When adding more segments, add ```f4, f5, ...``` in the ```filterList```
#%%[markdown]
# ### Run cell
#%%
f1 = sg.SegmentedFilter(start='2018-02-25 10:00:00', end='2018-03-08 02:00:00', a=500, b=0, c=1, d=1)
f2 = sg.SegmentedFilter(start='2018-03-08 02:00:00', end='2018-03-13 10:00:00', a=1, b=0.05, c=1, d=1)
f3 = sg.SegmentedFilter(start='2018-03-18 06:00:00', end='2018-03-29 09:00:00', a=-1, b=0, c=1, d=1)
filterList = [f1, f2, f3]
#%%[markdown]
# ### Run to apply segmented filter
#%%
data = df[measurementColumnName]
filterData = sg.applyFilters(df, timeColumnName, data, list(data), filterList)
sg.plotNotebook(xdata=(df[timeColumnName], 'time'), yinput=(data, measurementColumnName), yfilter=(filterData, 'filter'))
#%%[markdown]
# ### Run to export results from the previous cell. <br /> Set ```resultPath``` for the output file.
#%%
sDf = sg.getFiltersDf(timeColumnName, df[timeColumnName], measurementColumnName, data, filterData)
th.exportDf(sDf, 'resources/segmented-result-example.csv')